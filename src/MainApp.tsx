import { Layout, Menu, Popover, Typography } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { ME_SET_ME } from "./appRedux/ActionTypes";
import { History } from "./page/History";
import { Routes } from './route/Routes'

const { Header, Content, Footer } = Layout;

const MainApp = () => {
  const dispatch = useDispatch()
  return (
    <Layout
      className="layout"
      style={{ minHeight: '100vh' }}
    >
      <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
        <div className="logo">
          <Link to="/">
            <Typography.Title
              level={4}
              style={{ textAlign: 'center' }}
            >
              RENTBOOK
            </Typography.Title>
          </Link>
        </div>
        <Menu
          theme="dark"
          mode="horizontal"
        >
          <Menu.Item
            key={1}
            style={{ marginLeft: 'auto' }}
          >
            <Popover
              content={<History />}
              title="History / TimeLine"
              trigger="click"
            >
              <a
                href="#"
                onClick={(e) => {
                  e.preventDefault()
                }}
              >{`History / Timeline`}</a>
            </Popover>
          </Menu.Item>
          <Menu.Item key={2}>
            <a
              href="#"
              onClick={(e) => {
                e.preventDefault()
                dispatch({ type: ME_SET_ME, payload: {} })
              }}
            >{`Logout`}</a></Menu.Item>
        </Menu>
      </Header>
      <Content style={{ padding: '80px 50px' }}>
        <div className="site-layout-content">
          <Routes />
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>RentBook ©2022 Created by Pvstelles</Footer>
    </Layout>
  )
}

export default MainApp
