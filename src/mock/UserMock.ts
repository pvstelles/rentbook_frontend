export const UserMock = {
  1: {
    id: 1,
    name: 'Admin',
    email: 'admin@email.com',
    password: 'admin123',
    role: 'admin'
  },
  2: {
    id: 2,
    name: 'User',
    email: 'user@email.com',
    password: 'user123',
    role: 'user'
  }
}
