export const CategoryMock = {
  1: {
    id: 1,
    name: 'Technology',
  },
  2: {
    id: 2,
    name: 'Novel',
  },
  3: {
    id: 3,
    name: 'Kids',
  },
  4: {
    id: 4,
    name: 'Science fiction',
  }
}
