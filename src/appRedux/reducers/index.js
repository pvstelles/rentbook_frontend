import {combineReducers} from "redux";
import {connectRouter} from "connected-react-router";
import {TokenReducer} from "./Token";
import {StoreReducer} from "./Store";
import {MeReducer} from "./Me";

export default (history) => combineReducers({
    router: connectRouter(history),
    Token: TokenReducer,
    Store: StoreReducer,
    Me: MeReducer
});
