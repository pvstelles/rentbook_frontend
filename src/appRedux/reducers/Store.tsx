import { CategoryType } from '../../@type/CategoryType';
import { RentType } from '../../@type/RentType';
import { UserType } from '../../@type/UserType';
import { BookMock } from '../../mock/BookMock';
import { BookType } from '../../@type/BookType';
import { CategoryMock } from '../../mock/CategoryMock';
import { UserMock } from '../../mock/UserMock';
import { STORE_SET_STORE, STORE_CLEAR_STORE, STORE_CLEAR_STORES } from '../ActionTypes'
import _ from  'lodash'
const INIT_STATE = {
  book: BookMock as { [key: number]: BookType },
  user: UserMock as { [key: number]: UserType },
  category: CategoryMock as { [key: number]: CategoryType },
  rent: {} as { [key: number]: RentType },

}

export const StoreReducer = (state = INIT_STATE, action: any) => {
  switch (action.type) {
    case STORE_SET_STORE: {
      const model = action?.payload?.model
      const id = action?.payload?.data?.id || (_.maxBy<any>(Object.values(state?.[model] || {}), 'id')?.id || 1) + 1
      action.payload.data.id = id
      return {
        ...state,
        [model]: {
          ...state?.[model],
          [id]: action?.payload?.data
        }
      }
    }
    case STORE_CLEAR_STORE: {
      const model = action?.payload?.model
      return {
        ...state,
        [model]: _.mapKeys(_.filter(Object.values(state?.[model] || {}), (i: any) => i?.id !== action?.payload?.id), 'id')
      }
    }
    case STORE_CLEAR_STORES: {
      return INIT_STATE
    }
    default: {
      return state
    }
  }
}
