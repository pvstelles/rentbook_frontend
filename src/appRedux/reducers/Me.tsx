import { ME_SET_ME } from '../ActionTypes'

const INIT_STATE = {}

export const MeReducer = (state = INIT_STATE, action: any) => {
  switch (action.type) {
    case ME_SET_ME: {
      return action?.payload
    }
    default: {
      return state
    }
  }
}
