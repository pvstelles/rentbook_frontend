import { TOKEN_SET_TOKEN } from '../ActionTypes'

const INIT_STATE = {}

export const TokenReducer = (state = INIT_STATE, action: any) => {
  switch (action.type) {
    case TOKEN_SET_TOKEN: {
      return action?.payload
    }
    default: {
      return state
    }
  }
}
