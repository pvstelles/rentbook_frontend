import React from 'react'
import { Space } from 'antd';

export const IconText = ({text, icon}: any) => {
  return (
    <Space>
      {React.createElement(icon)}
      {text}
    </Space>
  )
}
