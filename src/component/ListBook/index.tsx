import { EditOutlined } from '@ant-design/icons';
import { Card, List } from 'antd';
import _ from 'lodash';
import React from 'react'
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { BookType } from '../../@type/BookType';
import { StoreType } from '../../@type/StoreType';

interface Props {
  match?: any
  books: BookType[];
}

export const ListBook: React.FC<Props> = ({ books }) => {
  const me = useSelector((state: StoreType) => state?.Me)

  return (
    <Card>
      <List
        bordered
        itemLayout="vertical"
        size="large"
        pagination={{
          onChange: page => {
            console.log(page);
          },
          pageSize: 3,
        }}
        dataSource={books}
        renderItem={(item: any) => (
          <List.Item
            key={item?.id}
            extra={
              <img
                width={150}
                alt="logo"
                src={item?.cover}
              />
            }
          >
            <List.Item.Meta
              title={(
                <>
                  <Link to={`/book/show/${item?.id}`}>{item?.title}</Link>
                  {me?.role === 'admin' ? (
                    <span style={{ marginLeft: 8 }}>
                      <Link to={`/book/${item?.id}`}><EditOutlined /></Link>
                    </span>
                  ) : null}
                </>
              )}
              description={<span
                dangerouslySetInnerHTML={{
                  __html: _.truncate(item?.description, {
                    length: 600,
                    omission: '[...]'
                  })
                }}
              />}
            />
            {item?.content}
          </List.Item>
        )}
      />
    </Card>
  )
}
