import { UploadOutlined } from '@ant-design/icons';
import { Button, Upload } from 'antd';
import React from 'react'

export const UploadComponent = ({...rest}) => {
  console.log(rest)
  return (
    <Upload {...rest}>
      <Button icon={<UploadOutlined />}>Select File</Button>
    </Upload>
  )
}
