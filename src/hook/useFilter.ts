import { useMemo, useState } from 'react'
import _ from 'lodash';
import { useSelector } from 'react-redux';
import { StoreType } from '../@type/StoreType';
import { _isBookAvailable, normalizeToFilter, searchString } from '../util/service';
const INITIAL_FILTER_STATE = {
  title: '',
  all: '',
  author: '',
  category: 0,
  available: false
}
export const useFilter = () => {

  const books = useSelector((state: StoreType) => Object.values(state?.Store?.['book'] || {}))
  const rents = useSelector((state: StoreType) => Object.values(state?.Store?.['rent'] || {}))
  const [filter, setFilter] = useState(INITIAL_FILTER_STATE)

  const booksFiltered = useMemo(() => {
    let filtered = books
    filtered = filter?.category ? _.filter(filtered, (item) => item?.category === filter?.category) : filtered
    filtered = searchString(filter?.title, 'title', filtered)
    filtered = searchString(filter?.author, 'author', filtered)
    console.log(filtered, 'dfd')
    filtered = filter?.available ? _.filter(filtered, (item) => _isBookAvailable(item?.id, rents)) : filtered


    if (filter?.all) {
      const filterAll = normalizeToFilter(filter?.all)
      filtered = _.filter(filtered, (item) => {
        const all = `${normalizeToFilter(item?.title)} ${normalizeToFilter(item?.author)} ${normalizeToFilter(item?.description)}`
        return all.indexOf(filterAll) > -1
      })
    }

    return filtered
  }, [filter, books])

  return { booksFiltered, filter, setFilter, INITIAL_FILTER_STATE }
}
