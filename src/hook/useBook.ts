import _, { parseInt } from 'lodash'
import moment from 'moment';
import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { BookType } from '../@type/BookType';
import { RentType } from '../@type/RentType';
import { StoreType } from '../@type/StoreType';
import { _isBookAvailable } from '../util/service';


export const useBook: ({ bookId }: { bookId: number | string }) => { book: BookType; isAvailableNow: any } = ({ bookId }) => {

  const book = useSelector((state: StoreType) => state?.Store?.book?.[bookId] || null)
  const rents: RentType[] = useSelector((state: StoreType) => _.filter(Object.values(state?.Store?.rent || {}), (rent: RentType) => rent?.book_id === parseInt(bookId + '')))

  const isAvailableNow = useMemo(() => _isBookAvailable(parseInt(bookId + ''), rents, moment()), [book, rents])

  return { book, isAvailableNow }
}
