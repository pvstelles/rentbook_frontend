import React from 'react'
import './App.css';
import 'antd/dist/antd.css'
import 'react-quill/dist/quill.snow.css';
import {Route, Switch} from "react-router-dom";
import {Initialize} from "./route/Initialize";
import configureStore, {history} from './appRedux/store';
import {PersistGate} from 'redux-persist/integration/react'
import {Provider} from 'react-redux'
import {ConnectedRouter} from 'connected-react-router'
const store = configureStore({});

function App() {
    return (
        <Provider store={store.store}>
            <PersistGate
                loading={false}
                persistor={store?.persistor}
            >
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route
                            path="/"
                            component={Initialize}
                        />
                    </Switch>
                </ConnectedRouter>
            </PersistGate>
        </Provider>

    );
}

export default App;
