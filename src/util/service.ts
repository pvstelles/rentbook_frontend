import _ from 'lodash';
import moment from 'moment';
import { RentType } from '../@type/RentType';

export const isBetween = (dateStart: any, dateEnd: any, dataCheck: any = moment()) => dataCheck.isBetween(moment(dateStart), moment(dateEnd))

export const isSame = (date: any, dataCheck: any = moment()) => dataCheck.isSame(moment(date), 'day')

export const _isBookAvailable = (bookId: any, rents: RentType[] = [], dataCheck: any = moment()) => {
  let available = true;
  _.filter(rents, (r) => r?.book_id === bookId).map((rent) => {
    if (isBetween(rent?.date_start, rent?.date_end, dataCheck) || isSame(rent?.date_start, dataCheck) || isSame(rent?.date_end, dataCheck)) {
      available = false
    }
    return;
  })
  return available
}

export const normalizeToFilter = (text: string = '') => {
  return _.lowerCase(text.normalize('NFD').replace(/([\u0300-\u036f]|[^0-9a-zA-Z\s])/g, ''))
}

export const searchString = (text: string = '', key: string = '', arr: any[]) => {
  if (!text) return arr

  text = normalizeToFilter(text)
  return _.filter(arr, (item) => normalizeToFilter(item?.[key]).indexOf(text) > -1)
}
