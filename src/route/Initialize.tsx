import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route, Switch, useRouteMatch } from "react-router-dom";
import { StoreType } from '../@type/StoreType';
import MainApp from "../MainApp";
import { Signin } from "../page/Signin";
import { Signup } from "../page/Signup";


const RestrictedRoute = ({ component: Component, token, ...rest }: any) =>
  <Route {...rest} render={props => token ? <Component {...props} /> : <Redirect to={'/signin'} />}
  />;

export const Initialize = () => {
  const match = useRouteMatch();
  const me = useSelector((state: StoreType) => state?.Me)

  return (
    <Switch>
      <Route
        exact
        path={'/signin'}
        component={() => <Signin />}
      />
      <Route
        path='/signup'
        component={() => <Signup />}
      />
      <Route
        path='/forgot-password'
        component={() => <span>ForgotPassword</span>}
      />
      <RestrictedRoute
        token={me?.id}
        path={`${match?.url}`}
        component={MainApp}
      />
    </Switch>
  )
}
