import React from 'react'
import { Route, Switch } from 'react-router-dom';
import { Book } from '../page/Book';
import { BookStore } from '../page/BookStore';
import { History } from '../page/History';
import { Home } from '../page/Home';

export const Routes = () => {
  return (
    <>
      <Switch>
        <Route
          exact
          path={'/'}
          component={(props: any) => <Home {...props} />}
        />
        <Route
          exact
          path={'/history'}
          component={(props: any) => <History {...props} />}
        />
        <Route
          exact
          path={'/book/show/:id'}
          component={(props: any) => <Book {...props} />}
        />
        <Route
          exact
          path={'/book/:id'}
          component={(props: any) => <BookStore  {...props} />}
        />
      </Switch>
    </>
  )
}
