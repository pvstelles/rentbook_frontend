import { Button, Card, Col, Form, Input, notification, PageHeader, Row, Typography } from 'antd';
import _ from 'lodash';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { StoreType } from '../../@type/StoreType';
import { UserType } from '../../@type/UserType';
import { ME_SET_ME, STORE_SET_STORE } from '../../appRedux/ActionTypes';

export const Signup = () => {
  const dispatch = useDispatch()
  const users = useSelector((state: StoreType) => Object.values(state?.Store?.user || {}))
  const history = useHistory();
  const [form] = Form.useForm()

  const validateEmail = (_t: any, value: any ) => {
    const userCheck = _.filter(users, (u: UserType) => u?.email === value)
    if (!userCheck?.length) {
      return Promise.resolve()
    }
    return Promise.reject(new Error('This email is unavailable'));
  }

  const onFinish = (values: any) => {

      const userAux = { ...values, role: 'user' }
      dispatch({ type: STORE_SET_STORE, payload: { model: 'user', data: userAux } })
      dispatch({ type: ME_SET_ME, payload: userAux })
      history.push('/')
      notification.success({
        message: 'Success!',
      })
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Row
      justify="center"
      align="middle"
      style={{ height: '100vh' }}
    >
      <Col span={16}>
        <Card>
          <PageHeader
            title={"Signup"}
            onBack={() => history.push('/signin')}
          />
          <Row>
            <Col xs={6}>
              <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%' }}>
                <Typography.Title level={1}>
                  RENT
                  <br />
                  BOOK
                </Typography.Title>
              </div>
            </Col>
            <Col xs={18}>
              <Form
                form={form}
                layout="vertical"
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
              >
                <Form.Item
                  label="Name"
                  name="name"
                  rules={[{ required: true, message: 'Please input your email!' }]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Email"
                  name="email"
                  rules={[
                    { required: true, message: 'Please input your email!' },
                    {validator: validateEmail}
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Password"
                  name="password"
                  rules={[{ required: true, message: 'Please input your password!' }]}
                >
                  <Input.Password />
                </Form.Item>

                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                  >
                    Submit
                  </Button>
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </Card>
      </Col>
    </Row>
  )
}
