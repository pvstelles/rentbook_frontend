import { Button, Card, Checkbox, Col, Form, Input, notification, PageHeader, Row, Typography } from 'antd';
import _ from 'lodash'
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { StoreType } from '../../@type/StoreType';
import { UserType } from '../../@type/UserType';
import { ME_SET_ME } from '../../appRedux/ActionTypes';

export const Signin = () => {
  const dispatch = useDispatch()
  const users = useSelector((state: StoreType) => Object.values(state?.Store?.user || {}))
  const history = useHistory();
  const [form] = Form.useForm()

  const onFinish = (values: any) => {
    const user = _.filter(users, (user: UserType) => user?.email === values?.email && user?.password === values?.password)
    console.log(user, users)
    if (user?.length) {
      dispatch({ type: ME_SET_ME, payload: user[0] })
      history.push('/')
    } else {
      notification.error({
        message: 'Ops!',
        description: 'User or password invalid'
      })
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Row
      justify="center"
      align="middle"
      style={{ height: '100vh' }}
    >
      <Col span={16}>
        <Card>
          <PageHeader
            title={"Signin"}
          />
          <Row>
            <Col xs={6}>
              <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%' }}>
                <Typography.Title level={1}>
                  RENT
                  <br />
                  BOOK
                </Typography.Title>
              </div>
            </Col>
            <Col xs={18}>
              <Form
                form={form}
                layout="vertical"
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
              >
                <Form.Item
                  label="Email"
                  name="email"
                  rules={[{ required: true, message: 'Please input your email!' }]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Password"
                  name="password"
                  rules={[{ required: true, message: 'Please input your password!' }]}
                >
                  <Input.Password />
                </Form.Item>

                <Form.Item
                  name="remember"
                  valuePropName="checked"
                >
                  <Checkbox>Remember me</Checkbox>
                </Form.Item>

                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                  >
                    Submit
                  </Button>
                </Form.Item>
                <Form.Item>
                  <Link
                    to="/signup"
                  >
                    Don't have a registration yet?
                  </Link>
                </Form.Item>
                <Form.Item>
                  <Row gutter={9}>
                    <Col xs={4}>
                      <Button
                        type="default"
                        onClick={() => form.setFieldsValue({ email: 'admin@email.com', password: 'admin123' })}
                      >
                        AutoFill Admin
                      </Button>
                    </Col>
                    <Col>
                      <Button
                        type="default"
                        onClick={() => form.setFieldsValue({ email: 'user@email.com', password: 'user123' })}
                      >
                        AutoFill user
                      </Button>
                    </Col>
                  </Row>
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </Card>
      </Col>
    </Row>
  )
}
