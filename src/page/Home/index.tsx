import { Button, Card, Col, PageHeader, Row } from 'antd';
import React from 'react'
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { StoreType } from '../../@type/StoreType';
import { ListBook } from '../../component/ListBook';
import { FormFilter } from '../../form/FormFilter';
import { useFilter } from '../../hook/useFilter';


export const Home = () => {
  const history = useHistory();
  const me = useSelector((state: StoreType) => state?.Me)

  const { booksFiltered, setFilter, INITIAL_FILTER_STATE } = useFilter()

  return (
    <React.Fragment>
      <PageHeader
        title={`Welcome, ${me?.name}!`}
        extra={me?.role === 'admin' ? [(
          <Button key={"1"} type="primary" onClick={() => history.push('/book/store')}>
            New Book
          </Button>
        )] : []}
      />
      <Row gutter={9}>
        <Col
          xs={24}
          md={{ span: 6 }}
          lg={{ span: 5 }}
        >
          <Card title={'Filter'}>
            <FormFilter
              setFilter={setFilter}
              initialFilterState={INITIAL_FILTER_STATE}
            />
          </Card>

        </Col>
        <Col
          xs={24}
          md={{ span: 16, offset: 1 }}
          lg={{ span: 18, offset: 1 }}
        >
          <ListBook books={booksFiltered} />
        </Col>
      </Row>
    </React.Fragment>
  )
}
