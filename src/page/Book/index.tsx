import { Button, Card, Col, Image, PageHeader, Row, Tag, Typography } from 'antd';
import _ from 'lodash';
import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { StoreType } from '../../@type/StoreType';
import { FormRent } from '../../form/FormRent';
import { useBook } from '../../hook/useBook';

interface Props {
  match?: any
}

export const Book: React.FC<Props> = ({ match }) => {
  const history = useHistory()
  const { book } = useBook({ bookId: match?.params?.id || '' })
  const categories = useSelector((state: StoreType) => state?.Store?.category || {})
  const [readyMore, setReadyMore] = useState(false)

  return (
    <>
      <PageHeader
        title="back"
        onBack={() => history.goBack()}
      />
      <Card>
        <Row gutter={9}>
          <Col
            sm={24}
            md={12}
            lg={6}
          >
            <Image
              width={'100%'}
              src={book?.cover}
            />
          </Col>
          <Col
            sm={24}
            md={12}
            lg={{span: 17, offset: 1}}
          >
            <Typography.Title level={1}>{book?.name}</Typography.Title>
            <Typography.Title level={3} disabled>by: {book?.author}</Typography.Title>
            <Tag>{categories[book?.category]?.name}</Tag>

            <Card style={{ marginTop: 40 }}>
              <FormRent book={book} />
              <div dangerouslySetInnerHTML={{
                  __html: readyMore ? book?.description : _.truncate(book?.description, {
                    length: 200,
                    omission: '[...]'
                  })
                }} />
              <div>
                <Button
                  type="link"
                  onClick={() => setReadyMore(!readyMore)}
                >{readyMore ? 'show less' : 'read more'}</Button>
              </div>
            </Card>
          </Col>
        </Row>
      </Card>
    </>
  )
}
