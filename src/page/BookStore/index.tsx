import { Alert, Button, Card, Col, Form, Input, Modal, notification, PageHeader, Popconfirm, Row, Select } from 'antd';
import _ from 'lodash';
import React, { useMemo } from 'react'
import ReactQuill from 'react-quill';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { StoreType } from '../../@type/StoreType';
import { STORE_CLEAR_STORE, STORE_SET_STORE } from '../../appRedux/ActionTypes';
import { UploadComponent } from '../../component/UploadComponent';
import { useBook } from '../../hook/useBook';

interface Props {
  match?: any
}

const normFile = (e: any) => {
  console.log('Upload event:', e);
  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};

export const BookStore: React.FC<Props> = ({ match }) => {
  const history = useHistory();
  const me = useSelector((state: StoreType) => state?.Me)
  const dispatch = useDispatch();
  const { book, isAvailableNow } = useBook({ bookId: match?.params?.id })
  const categories = useSelector((state: StoreType) => Object.values(state?.Store?.category || {}))
  const onFinish = (values: any) => {
    if (isAvailableNow) {
      const cover = values?.cover?.length ? (values?.cover[0]?.thumbUrl || values?.cover[0]?.url) : book?.cover
      dispatch({
        type: STORE_SET_STORE, payload: {
          model: 'book',
          data: { ...book, ...values, cover }
        }
      })
      history.push('/')
      notification.success({
        message: 'Success!',
        description: `The book was ${book?.id ? 'updated' : 'stored'}!`
      })
    }
  }

  const initialValues = useMemo(() => {
    return {
      ...book, cover: book?.cover ? [{
        uid: '1',
        name: 'xxx.png',
        status: 'done',
        url: book?.cover,
      }] : [], description: book?.description || ''
    }
  }, [book])

  if (me?.role !== 'admin') {
    return (
      <Modal
        visible={true}
        onCancel={() => history.goBack()}
        onOk={() => history.goBack()}
      >
        Ops... Invalid access!
      </Modal>
    )
  }

  return (
    <Row gutter={9}>
      <Col span={24}>
        <PageHeader
          onBack={() => history.push('/')}
          title={`${_.isNaN(parseInt(match?.params?.id)) ? 'Store' : 'Update'} Book`}
        />
      </Col>
      {!isAvailableNow ? (
        <Col span={24}>
          <Alert
            showIcon
            message="Attention!"
            description="Unable to edit a book that is in rented status"
            type="info"
            closable
          />
        </Col>
      ) : null}
      <Col span={24}>
        <Card style={{ marginTop: 32 }}>
          <Form
            layout="vertical"
            name="basic"
            initialValues={initialValues}
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              label="Cover"
              name="cover"
              valuePropName="fileList"
              getValueFromEvent={normFile}
              rules={[{ required: true }]}
            >
              <UploadComponent
                name={'cover'}
                disabled={!isAvailableNow}
                beforeUpload={() => false}
                maxCount={1}
                listType={"picture"}
              />
            </Form.Item>
            <Form.Item
              label="Title"
              name="title"
              rules={[{ required: true }]}

            >
              <Input
                disabled={!isAvailableNow}
              />
            </Form.Item>
            <Form.Item
              label="Author"
              name="author"
              rules={[{ required: true }]}

            >
              <Input
                disabled={!isAvailableNow}
              />
            </Form.Item>
            <Form.Item
              label="Category"
              name="category"
              rules={[{ required: true }]}

            >
              <Select
                disabled={!isAvailableNow}
              >
                {categories.map((cat) => (
                  <Select.Option
                    value={cat?.id}
                    key={cat?.id}
                  >{cat?.name}</Select.Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              label="Description"
              name="description"
              rules={[{ required: true }]}

            >
              <ReactQuill
                id={'description'}
                theme="snow"
                readOnly={!isAvailableNow}
              />
            </Form.Item>

            <Form.Item>
              <Row gutter={9}>
                <Col>
                  <Button
                    type="primary"
                    htmlType="submit"
                    disabled={!isAvailableNow}
                  >
                    Save
                  </Button>
                </Col>
                {book?.id ? (
                  <Col>
                    <Popconfirm
                      disabled={!isAvailableNow}
                      title="Are you sure？"
                      okText="Yes"
                      cancelText="No"
                      onConfirm={() => {
                        dispatch({ type: STORE_CLEAR_STORE, payload: { model: 'book', id: book?.id } })
                        notification.success({
                          message: 'Success!',
                          description: 'The book was deleted!'
                        })
                        history.push('/')
                      }}
                    >
                      <Button
                        type="link"
                        disabled={!isAvailableNow}
                      >Delete</Button>
                    </Popconfirm>
                  </Col>
                ) : null}
              </Row>
            </Form.Item>

          </Form>
        </Card>
      </Col>
    </Row>
  )
}
