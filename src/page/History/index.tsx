import { Button, Empty, Timeline, Typography } from 'antd';
import _ from 'lodash'
import moment from 'moment';
import {Scrollbars} from "react-custom-scrollbars";
import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import { RentType } from '../../@type/RentType';
import { StoreType } from '../../@type/StoreType';

export const History = () => {

  const me = useSelector((state: StoreType) => state?.Me)
  const rents = useSelector((state: StoreType) => _.filter(Object.values(state?.Store?.rent || {}), (r: RentType) => r?.user_id === me?.id).reverse())
  const books = useSelector((state: StoreType) => state?.Store?.book || {})
  const [quantityShow, setQuantityShow] = useState(3)

  return rents?.length ? (
    <Scrollbars
      style={{height: 500, width: 400}}
      autoHide
      renderTrackHorizontal={props => <div {...props}
                                           style={{ display: 'none' }}
                                           className="track-horizontal"
      />}
      children={(
        <Timeline mode={'alternate'} style={{padding: 8}}>
          {_.take(rents, quantityShow)?.map((r: RentType) => (
            <Timeline.Item key={r?.id} label={moment(r?.rented_at).fromNow()}>
              <Typography.Text>
                {'A book was rented / scheduled'}
              </Typography.Text>
              <br />
              <Typography.Text strong>
                {books?.[r?.book_id]?.title}
              </Typography.Text>
              <br />
              <Typography.Text disabled>
                {` by: ${books?.[r?.book_id]?.author}`}
              </Typography.Text>

              <br />
              <br />
              <Typography.Text type={"secondary"} underline>
                {moment(r?.date_start).format('MM/DD/YYYY')} until {moment(r?.date_end).format('MM/DD/YYYY')}
              </Typography.Text>
            </Timeline.Item>
          ))}
          {rents?.length > quantityShow ? (
            <Button
              type={"link"}
              onClick={() => setQuantityShow(quantityShow + quantityShow)}
            >Show more</Button>
          ) : null}
        </Timeline>
      )}
    />
  ) : (
    <Empty />
  )
}
