import { BookType } from './BookType';
import { CategoryType } from './CategoryType';
import { RentType } from './RentType';
import { UserType } from './UserType';

export interface StoreType {
  Store: {
    book: {[key: number]: BookType},
    user: {[key: number]: UserType},
    rent: {[key: number]: RentType},
    category: {[key: number]: CategoryType},

  },
  Me: UserType
}
