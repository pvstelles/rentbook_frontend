export interface BookType {
  id: number;
  name: string;
  title: string;
  description: string;
  category: number;
  cover: string;
  author: string;
}
