export interface RentType {
  id: number;
  book_id: number;
  user_id: number;
  date_start: any;
  date_end: any;
  rented_at: any;
}
