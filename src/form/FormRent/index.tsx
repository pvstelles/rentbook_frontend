import { Badge, Button, Calendar, Col, DatePicker, Form, Modal, notification, Row } from 'antd';
import _ from 'lodash';
import moment from 'moment';
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { BookType } from '../../@type/BookType';
import { RentType } from '../../@type/RentType';
import { StoreType } from '../../@type/StoreType';
import { STORE_SET_STORE } from '../../appRedux/ActionTypes';
import { _isBookAvailable } from '../../util/service';

interface Props {
  book: BookType
}

export const FormRent: React.FC<Props> = ({ book }) => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const me = useSelector((state: StoreType) => state?.Me)
  const history = useHistory();
  const [modalSchedule, setModalSchedule] = useState(false)

  const rent = useSelector((state: StoreType) => _.filter(Object.values(state?.Store?.rent || {}), (r: RentType) => r?.book_id === book?.id))

  const isBetween = (dateRent: any, dateStart: any, dateEnd: any) => {
    return dateRent.isBetween(dateStart, dateEnd) || dateRent.isSame(dateStart, 'day') || dateRent.isSame(dateEnd, 'day')
  }

  const canRent = () => {
    let can = true;
    const values = form.getFieldsValue()
    if (rent && rent?.length) {
      _.map(rent, (r: RentType) => {
        if (!values?.period?.[0].isSameOrAfter(moment(), 'day') || isBetween(moment(r?.date_start), values?.period?.[0], values?.period?.[1]) || isBetween(moment(r?.date_end), values?.period?.[0], values?.period?.[1])) {
          can = false
        }
      })
    } else if (!values?.period?.[0].isSameOrAfter(moment(), 'day')) {
      can = false
    }
    return can
  }

  const onFinish = (values: any) => {
    if (canRent()) {
      dispatch({
        type: STORE_SET_STORE, payload: {
          model: 'rent',
          data: {
            date_start: values?.period[0].format('YYYY-MM-DD'),
            date_end: values?.period[1].format('YYYY-MM-DD'),
            user_id: me?.id,
            book_id: book?.id,
            rented_at: moment().format('MM/DD/YYYY HH:mm:ss')
          }
        }
      })
      notification.success({
        message: 'Success!',
        description: 'A book was scheduled / rented'
      })
      history.push('/')
    } else {
      notification.error({
        message: 'Ops!',
        description: 'Invalid date, check the schedule!'
      })
    }

  };

  const dateCellRender = (value: any) => {
    const isAvailable = _isBookAvailable(book?.id, rent, value)
    if (isAvailable) return null
    return (
      <ul className="events">
        <li key={1}>
          <Badge
            status='success'
            text={'Unavailable'}
          />
        </li>
      </ul>
    );
  }

  return (
    <React.Fragment>
      <Form
        layout="vertical"
        name="basic"
        onFinish={onFinish}
        autoComplete="off"
        form={form}
      >
        <Form.Item
          label="Period"
          name="period"
          rules={[{ required: true, message: 'Please input an range date to rent!' }]}
        >
          <DatePicker.RangePicker />
        </Form.Item>
        <Form.Item>
          <Row gutter={9}>
            <Col>
              <Button
                type={'primary'}
                htmlType={"submit"}
              >
                Rent
              </Button>
            </Col>
            <Col>
              <Button
                type={'default'}
                onClick={() => setModalSchedule(true)}
              >
                Check Schedule
              </Button>
            </Col>
          </Row>
        </Form.Item>
      </Form>
      <Modal
        width={'80%'}
        visible={modalSchedule}
        onCancel={() => setModalSchedule(false)}
        footer={[]}
      >
        <Row>
          <Col style={{padding: 24}}>
            <Calendar
              dateCellRender={dateCellRender}
            />
          </Col>
        </Row>
      </Modal>
    </React.Fragment>
  )
}
