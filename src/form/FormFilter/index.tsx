import { Button, Col, Form, Input, Radio, Switch } from 'antd';
import React from 'react'
import { useSelector } from 'react-redux';
import { StoreType } from '../../@type/StoreType';

interface Props {
  setFilter: any
  initialFilterState: any
}

export const FormFilter: React.FC<Props> = ({ setFilter, initialFilterState }) => {
  const [form] = Form.useForm();
  const categories = useSelector((state: StoreType) => Object.values(state?.Store?.category || {}))

  const onFinish = (values: any) => {
    setFilter(values)
  };

  const onFinishFailed = () => {

  };

  return (
    <React.Fragment>
      <Form
        layout="vertical"
        name="basic"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        form={form}
      >
        <Form.Item
          label="All"
          name="all"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Title"
          name="title"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Author"
          name="author"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Available now!"
          name="available"
          valuePropName={'checked'}
        >
          <Switch />
        </Form.Item>
        <Form.Item
          label="Category"
          name="category"
        >
          <Radio.Group>
            {categories.map((cat) => (
              <Col key={cat?.id}>
                <Radio value={cat?.id}>{cat?.name}</Radio>
              </Col>
            ))}
          </Radio.Group>
        </Form.Item>
        <Form.Item>
          <Button
            block
            type={'primary'}
            htmlType={"submit"}
          >
            Filter
          </Button>
        </Form.Item>
        <Form.Item>
          <Button
            block
            type={'default'}
            onClick={() => {
              form.setFieldsValue(initialFilterState)
              setFilter(initialFilterState)
            }}
          >
            Clear Filter
          </Button>
        </Form.Item>
      </Form>
    </React.Fragment>
  )
}
