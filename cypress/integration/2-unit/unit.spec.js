import * as service from '../../../src/util/service'
import moment from "moment";

describe('example to-do app', () => {
    context('service.ts', function () {
        it('normalize string', function () {
            expect(service.normalizeToFilter('Páginã')).to.eq('pagina')
        })
        it('date is between range', function () {
            expect(service.isBetween(moment('2022-05-01'), moment('2022-05-30'), moment('2022-05-10'))).to.true
        })
        it('date is not between range', function () {
            expect(service.isBetween(moment('2022-05-01'), moment('2022-05-30'), moment('2022-06-10'))).to.false
        })

        it('date is same day', function () {
            expect(service.isSame(moment('2022-05-01'), moment('2022-05-01'))).to.true
        })
        it('date is not same day', function () {
            expect(service.isSame(moment('2022-05-01'), moment('2022-05-02'))).to.false
        })

    })
})
