const book = {
  title: 'Test Store Book',
  author: 'cypress',
  category: 1,
  description: 'description write by cypress'
}
describe('test rent book app', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000')
  })

  it('user can signin', () => {
    cy.login('admin@email.com', 'admin123')
    cy.contains('Welcome, Admin!')
  })

  it('visit all pages', () => {

    cy.contains('Signin').should('be.visible')
    cy.contains('Don\'t have a registration yet').should('be.visible')
    cy.contains('Don\'t have a registration yet').click()
    cy.contains('Signup').should('be.visible')
    cy.get('.ant-page-header-back-button > .anticon > svg').click()
    cy.login('admin@email.com', 'admin123')
    cy.get('.ant-space-item > .ant-btn').click()
    cy.get('.ant-page-header-back-button > .anticon > svg').click()
    cy.get(':nth-child(1) > .ant-list-item-main > .ant-list-item-meta > .ant-list-item-meta-content > .ant-list-item-meta-title > [style="margin-left: 8px;"] > a > .anticon > svg').click()
    cy.get('.ant-page-header-back-button > .anticon > svg').click()
    cy.get('[href="/book/show/1"]').click()
    cy.get(':nth-child(2) > .ant-btn').click()
    cy.get('.ant-modal-close-x').click()
    cy.get('.ant-page-header-back-button > .anticon > svg').click()
    cy.contains('Logout').click()
    cy.contains('Signin').should('be.visible')
  })

  it('add new book', () => {
    cy.login('admin@email.com', 'admin123')
    cy.get('.ant-space-item > .ant-btn').click()
    cy.contains('Store Book')
    cy.get('#basic_cover').selectFile('cypress/assets/img.png', {force: true})
    cy.get('#basic_title').type(book.title)
    cy.get('#basic_author').type(book.author)
    cy.get('#basic_category').click()
    cy.contains('Kids').click()
    cy.get('.ql-editor').type(book.description)
    cy.get('.ant-col > .ant-btn').click()

    cy.contains('Success').should('be.visible')
    cy.get('#basic_author').type(book?.author)
    cy.get(':nth-child(6) > .ant-col > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-btn').click()
    cy.contains(book?.description).should('be.visible')
  })

  it('filter book by title', () => {
    cy.login('admin@email.com', 'admin123')
    cy.get('#basic_title').type('Hobbit')
    cy.get(':nth-child(6) > .ant-col > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-btn').click()
    cy.contains('O Hobbit').should('be.visible')
  })
})
